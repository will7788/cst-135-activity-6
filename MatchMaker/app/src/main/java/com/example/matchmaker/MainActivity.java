package com.example.matchmaker;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button b_compute;
    ImageView iv_needle;
    EditText et_yourName, et_OtherPersonName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_compute = (Button) findViewById(R.id.b_compute);
        iv_needle = (ImageView) findViewById(R.id.iv_needle);
        et_OtherPersonName = (EditText) findViewById(R.id.et_otherpersonname);
        et_yourName = (EditText) findViewById(R.id.et_yourname);

        b_compute.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                String yourName = et_yourName.getText().toString();
                String otherPersonName = et_OtherPersonName.getText().toString();

                int totalLetters = yourName.length() + otherPersonName.length();
                int totalMatches = 0;
                for (int i = 0; i <yourName.length(); i++)
                {
                    for(int j = 0; i<otherPersonName.length(); i++)
                    {
                        if (yourName.charAt(i) == otherPersonName.charAt(j))
                        {
                            totalMatches++;
                        }
                    }
                }

                for (int i = 0; i <otherPersonName.length(); i++)
                {
                    for(int j = 0; i<yourName.length(); i++)
                    {
                        if (otherPersonName.charAt(i) == yourName.charAt(j))
                        {
                            totalMatches++;
                        }
                    }
                }
                float compatScore = (float) totalMatches / totalLetters;
                Toast.makeText(MainActivity.this, "Compatibility Score: " + compatScore, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
